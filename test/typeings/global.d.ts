declare namespace NodeJS {
  export interface Global {
    apiBoilerplateConfig: any
    mongoosePromise: any
  }
}