import { TestService } from '../src/services/TestService'
import "reflect-metadata"
import { TestRepository } from '../src/repositories/TestRepository'
import { IRepository } from '../src/repositories/IRepository'
import { ITestModel } from '../src/models/ITestModel'
import container from '../src/inversify.config'
import TYPES from '../src/common/Types'
import { ConfigParams } from '../src/configs/ConfigParams';

declare let jest, describe, it, expect;

let testService:TestService

beforeAll(() => {
  global.apiBoilerplateConfig = ConfigParams.configParams()
  global.mongoosePromise = require('bluebird')
  testService = container.get<TestService>(TYPES.TestServce)
})

describe('TestService Suite', () => {

  it('should create new user', () => {
    return new Promise((resolve, reject)=> {
      
      testService.createTest({
        name: 'Created Test'
      }).then((result: any) => {
          expect(result).toBeDefined()
          expect(result.name).toEqual('Created Test')
          expect(result._id).toBeDefined()
        resolve()
      }).catch(err => {
        reject(err)
      })
    })
  });
}); 