import { IConfig } from './IConfig'
/**
 * @description
 */
export class QaConfig implements IConfig {
    public port = 8001
    public dbName = 'test'
    public mongoPort = 27017
    public mongoHost = 'localhost'
    public defaultFirstResultValue = 0
    public defaultMaxResultValue = 2
}