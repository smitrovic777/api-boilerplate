import { IConfig } from './IConfig'
import { DevelopmentConfig } from './DevelopmentConfig';
import { QaConfig } from './QaConfig';
import { ProductionConfig } from './ProductionConfig';

/**
 * @description
 */
export class ConfigParams {
    
    public static configParams (): IConfig {
        let envConfigParams = process.env.ENV || 'DEV'
        switch (envConfigParams) {
            case 'QA':
              return new QaConfig()
            
            case 'PROD':
              return new ProductionConfig()
            
            case 'DEV':
              return new DevelopmentConfig()

            default:
              throw Error('UNKNOWN_ENV_PARAM')
        }
    }

}