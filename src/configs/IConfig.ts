/**
 * @description
 */
export interface IConfig {
    port: number
    dbName: string
    mongoPort: number
    mongoHost: string
    defaultFirstResultValue: number
    defaultMaxResultValue: number
}