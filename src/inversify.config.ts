import { Container } from 'inversify'
import TYPES from './common/Types'
import { BaseController } from './controllers/BaseController'
import { TestController } from './controllers/TestController'
import { TestRepository } from './repositories/TestRepository'
import { TestService, TestServiceImpl } from './services/TestService'
import { IRepository } from './repositories/IRepository'
import { ITestModel } from './models/ITestModel'
import { Utils } from './common/Utils';

const container = new Container()

container.bind<BaseController>(TYPES.Controller).to(TestController)
container.bind<TestService>(TYPES.TestServce).to(TestServiceImpl)
container.bind<IRepository<ITestModel>>(TYPES.TestRepository).to(TestRepository)
container.bind<Utils>(TYPES.Utils).to(Utils)

export default container
