import { ITest } from './ITest'
import { Document } from 'mongoose'

/**
 * @description
 */
export interface ITestModel extends ITest, Document{

}