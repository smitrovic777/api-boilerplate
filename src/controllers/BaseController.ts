import * as express from 'express'
import { Response } from './../common/Response'
import { Data } from '../common/Data'
import { injectable } from 'inversify'
/**
 * @description
 */
@injectable()
export abstract class BaseController {

  /**
   * @description
   * @param app 
   */
  public abstract register(app: express.Express): void

  /**
   * @description
   * @param data 
   * @param res 
   */
  public sendReponse (data: Data, res: express.Response): express.Response {
    let response: Response = new Response()
    response.Status = data.Status
    response.Data = data.Data
    return res.status(response.Status).json(response)
  }
}
