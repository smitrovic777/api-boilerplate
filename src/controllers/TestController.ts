import * as express from 'express'
import { BaseController } from './BaseController'
import { Data } from '../common/Data'
import { injectable, inject} from 'inversify'
import TYPES from './../common/Types'
import { TestServiceImpl } from '../services/TestService'
import { ITest } from '../models/ITest'
import { Utils } from '../common/Utils'
/**
 * @description
 */
@injectable()
export class TestController extends BaseController {

    config = global.apiBoilerplateConfig
    
    @inject(TYPES.TestServce)
    private testService: TestServiceImpl 
    
    @inject(TYPES.Utils)
    private utils: Utils
    
    public register(app: any): void {
        app.route('/test')
            .get(async (req: express.Request, res: express.Response, next: express.NextFunction) => {
                    let firstResult = Number(req.query.first_result)
                    let maxResult = Number(req.query.max_result)
                    let sortPropertyName: string = this.utils.isNotNullOrUndefined(req.query.sort_attribute) ? String(req.query.sortAttribute) : 'name' 
                    let sortAttributeData: any = {}
                    let asc: number = this.utils.ascendingTrue(req.query.asc)
                    let diff = maxResult-firstResult
                    if (!this.utils.isNotNullOrUndefined(firstResult) || !this.utils.isPositiveNumber(firstResult) ||
                        !this.utils.isNotNullOrUndefined(maxResult) || !this.utils.isPositiveNumber(maxResult) ||
                        !this.utils.isNotNullOrUndefined(diff) || !this.utils.isPositiveNumber(diff)) {
                        firstResult = this.config.defaultFirstResultValue
                        maxResult = this.config.defaultMaxResultValue
                    }
                    try{
                        sortAttributeData[sortPropertyName] = asc
                        const tests = await this.testService.getTests(firstResult, maxResult, sortAttributeData)
                        this.sendReponse(new Data(200, tests), res)
                    } catch(err) {
                        return next(err)
                    }
            })
            .post(async (req: express.Request, res: express.Response, next: express.NextFunction) => {
                try{
                  const test: ITest = {
                      name: req.body.name
                  }
                  const createdTest = await this.testService.createTest(test)
                  this.sendReponse(new Data(201, createdTest), res)
                } catch(err) {
                  return next(err)
                }
            })
        app.route('/test/:id')
            .get(async (req: express.Request, res: express.Response, next: express.NextFunction) => {
                  const id = req.params.id
                    try{
                        let test = await this.testService.getTest(id)
                        this.sendReponse(new Data(200, test), res)
                    } catch (err) {
                        return next(err)
                    }
            })
            .put(async (req: express.Request, res: express.Response, next: express.NextFunction) => {
                const id: string | number = req.params.id
                try {
                    let test: ITest = {
                        name: req.body.name
                    }
                    let updatedTest = await this.testService.updateTest(id, test)
                    this.sendReponse(new Data(200, updatedTest), res)
                }catch (err) {
                    return next(err)
                }
            })
            
    }

}