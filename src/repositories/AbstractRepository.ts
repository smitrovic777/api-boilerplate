import { IRepository } from './IRepository'
import * as  mongoose from 'mongoose'
import { Document, Schema, Model, Connection} from 'mongoose'
import { injectable } from 'inversify';
import { BaseError } from '../common/BaseError';

/**
 * @description
 */
@injectable()
export abstract class AbstractRepository<TModel extends Document> implements IRepository<TModel> {
    public config = global.apiBoilerplateConfig
    public model: Model<TModel> 
    private connection: Connection 

    constructor () {
      this.connection = mongoose.createConnection('mongodb://' + this.config.mongoHost + ':' + this.config.mongoPort + '/' + this.config.dbName)
    }

    /**
     * @description
     * @param schema
     */
    public initModel(schema: Schema, modelName: string) {
       this.model = this.connection.model<TModel>(modelName, schema)
    }

    /**
     * @description
     */
    public async findAll(firstResult: number, maxResult: number, sortAttribute: Object): Promise<TModel[]> {
        return await this.model.find()
                               .skip(firstResult)
                               .limit(maxResult)
                               .sort(sortAttribute)
                               .exec()
                                .catch( err => {
                                    throw new BaseError(400, 'DATA_ACCESS_ERROR')
                                })
    }

    /**
     * @description
     * @param id
     */
    public async find(id: string | number): Promise<TModel | null> {
        return await this.model.findById(id).exec()
            .catch( err => {
                throw new BaseError(400, 'DATA_ACCESS_ERROR')
            })
    }

    /**
     * @description
     * @param t
     */
    public async update(id: string | number, t: TModel): Promise<TModel | null> {
        return await this.model.findByIdAndUpdate(id, t, {new: true}).exec()
            .catch( err => {
                throw new BaseError(400, 'DATA_ACCESS_ERROR')
            })
    }

    /**
     * @description
     * @param t
     */
    public async persist(t: TModel): Promise<TModel> {
        return await new this.model(t).save()
            .catch( err => {
                throw new BaseError(400, 'DATA_ACCESS_ERROR')
            })
    }
    
    /**
     * @description
     * @param t
     */
    public async delete(t: TModel): Promise<TModel> {
        return await new this.model(t).remove()
            .catch( err => {
                throw new BaseError(400, 'DATA_ACCESS_ERROR')
            })
    }

}