/**
 * @description
 */
export interface IRepository<T> {
    findAll(firstResult: number, maxResult: number, sortAttribute: Object): Promise<T[]>
    find(id: string): Promise<T | null>
    update(id: string | number,t: T): Promise<T | null>
    persist(t: T): Promise<T>
    delete(t: T): Promise<T>
}