import { injectable } from 'inversify'
import { AbstractRepository } from './AbstractRepository'
import { ITestModel } from '../models/ITestModel'
import testSchema from '../schemas/TestSchema'
/**
 * @description
 */
@injectable()
export class TestRepository extends AbstractRepository<ITestModel> {

    constructor () {
        super()
        this.initModel(testSchema, 'Test')
    }
}