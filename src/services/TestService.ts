import { injectable, inject } from 'inversify'
import { ITest } from '../models/ITest'
import TYPES from '../common/Types';
import { TestRepository } from '../repositories/TestRepository';

export interface TestService {
    createTest(test: ITest): Promise<ITest>
    updateTest(id: string | number,test: ITest): Promise<ITest | null>
    deleteTest(test: ITest): Promise<ITest>
    getTests(firstResult: number, maxResult: number, sortAttribute: Object): Promise<ITest[]>
    getTest(id: string | number): Promise<ITest | null>
}

@injectable()
export class TestServiceImpl implements TestService {

    constructor(@inject(TYPES.TestRepository) private testRepository: TestRepository){
        this.testRepository = testRepository
    }

    /**
     * @description
     * @param test 
     */
    public async createTest(test: ITest): Promise<ITest> {
        return await this.testRepository.persist(test as any)
    }

    /**
     * @description
     * @param test 
     */
    public async updateTest(id: string | number, test: ITest): Promise<ITest | null> {
        return await this.testRepository.update(id, test as any)
    }

    /**
     * @description
     * @param test 
     */
    public async deleteTest(test: ITest): Promise<ITest> {
        return await this.testRepository.delete(test as any)
    }
    /**
     * @description
     * @param firstResult 
     * @param maxResult 
     * @param sortAttribute 
     */
    public async getTests(firstResult: number, maxResult: number, sortAttribute: Object): Promise<ITest[]> {
        return await this.testRepository.findAll(firstResult, maxResult, sortAttribute)
    }

    /**
     * @description
     * @param id 
     */
    public async getTest(id: string | number): Promise<ITest | null> {
        return await this.testRepository.find(id)
    }

}