import { Schema } from "mongoose"

let testSchema: Schema = new Schema({
  name: String
},{
  timestamps: true
});

export default testSchema
