import 'reflect-metadata'
import 'es7-shim'
import container from './inversify.config'
import * as express from 'express'
import * as morgan from 'morgan'
import * as bodyParser from 'body-parser'
import TYPES from './common/Types'
//import { IKeycloakWrapper } from "./common/keycloak";
import { IConfig } from './configs/IConfig'
import { Response } from './common/Response';
import mongoose = require("mongoose")
import { BaseController } from './controllers/BaseController'
import { ConfigParams } from './configs/ConfigParams';
import { logger } from './common/Logger'

export class Main {

    private static app: express.Express

    static bootstrap(config: IConfig): express.Express {


        this.app = express()

        // setup the server
        //  1. setup morgan
        this.app.use(morgan('dev'))

        //  2. setup response headers 
        this.app.use(bodyParser.json())
        
        //  4. setup controllers
        const controllers: BaseController[] = container.getAll<BaseController>(TYPES.Controller)
        controllers.forEach(controller => controller.register(this.app))

        this.app.use((err: Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
            let error = err as any
            let response: Response = new Response()
            response.Status = error.status
            response.ErrorMessage = error.errorMessage 
            return res.status(response.Status).json(response)
        });

        // start the server
        this.app.listen(config.port)

        logger.debug('Server is listening on: ', config.port)

        return this.app
    }
}

global.apiBoilerplateConfig = ConfigParams.configParams()
global.mongoosePromise = require('bluebird');
mongoose.Promise = global.mongoosePromise;

Main.bootstrap(global.apiBoilerplateConfig)