export class Data {
    private status: number 
    private data: any

    constructor (status: number, data: any) {
        this.status = status
        this.data = data
    }

    get Status () {
        return this.status
    }

    get Data () {
        return this.data
    }
}