import * as winston from 'winston'

// usage: logger.debug('text here')
export const logger = new (winston.Logger)({
  transports: [
    new winston.transports.Console({
      level: 'debug',
      timestamp: true,
      handleExceptions: true,
      json: false,
      colorize: true
    }),
  ]
})