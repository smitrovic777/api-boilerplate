import { injectable } from 'inversify'

@injectable()
export class Utils {

    /**
     * @description
     * @param param 
     */
    public isNotNullOrUndefined(param: any):boolean {
        return param !== undefined && param !== null
    }

    /**
     * @description
     * @param param 
     */
    public isPositiveNumber (param: number): boolean {
        return !isNaN(param) && param >= 0
    }

    /**
     * @description
     * @param asc 
     */
    public ascendingTrue (asc: string): number {
        return asc === 'true' ? 1 : -1
    }
}