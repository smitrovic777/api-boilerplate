/**
 * @description
 * 
 * Represents the types for dependency injection
 * - runtime identifiers
 */
const TYPES = {

    Controller: Symbol('Controller'),
    TestServce: Symbol('TestService'),
    TestRepository: Symbol('TestRepository'),
    Utils: Symbol('Utils')

}

export default TYPES