import { Data } from './Data'

export class Response {
    private status: number
    private errorMessage?: string
    private data?: Data

    get Status () {
        return this.status
    }

    set Status (value: number) {
        this.status = value
    }

    get ErrorMessage () {
        return this.errorMessage
    }

    set ErrorMessage (value: string | undefined) {
        this.errorMessage = value
    }

    get Data () {
        return this.data
    }

    set Data (value: Data | undefined) {
        this.data = value
    }
}