export class BaseError extends Error {
    
    private errorCode: number
    private errorMessage: string

    constructor (errorCode: number, errorMessage: string) {
        super()
        this.errorCode = errorCode
        this.errorMessage = errorMessage
    }

    get ErrorCode () {
        return this.errorCode
    }

    get ErrorMessage () {
        return this.errorMessage
    }
}