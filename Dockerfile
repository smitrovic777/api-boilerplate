# Use Node.js 6.9 (LTS)
FROM node:boron

# Create the directory for the app.
RUN mkdir -p /usr/src/app

# Install the 'webpack' module globally.
RUN npm install -g webpack

# Install the 'typescript' module globally.
RUN npm install -g typescript

# Install the 'supervisor' module globally.
RUN npm install -g supervisor

# Grant permissions to the 'node' user.
WORKDIR /usr/src/app
RUN chown -R node:node /usr/src/app

# Install the dependencies.
USER node
COPY . /usr/src/app

# Root privileges needed to run `npm install`
USER root
RUN npm install
USER node

# Start the app on port 3003.
EXPOSE 3021
